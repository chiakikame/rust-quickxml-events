extern crate quick_xml;
extern crate error_chain;
extern crate clap;

use quick_xml::reader::Reader;
use quick_xml::events::*;
use error_chain::ChainedError;
use std::io::{stdin, BufRead};
use clap::App;

/// Main
fn main() {
    let options = get_options_from_command_line();
    if options.input_file.is_none() {
        let stdin = stdin();
        process_stream(stdin.lock(), &options);
    } else {
        use std::fs::File;
        use std::path::Path;
        use std::io::BufReader;

        let input_file = options.input_file.as_ref().unwrap();
        let stream = File::open(Path::new(&input_file)).expect("Cannot open file");
        let buffered = BufReader::new(stream);
        process_stream(buffered, &options);
    }
}

fn process_stream<B>(stream: B, options: &Options)
where
    B: BufRead,
{
    let mut buffer = vec![];
    let mut reader = Reader::from_reader(stream);
    let mut path: Vec<String> = Vec::new();

    loop {
        match reader.read_event(&mut buffer) {
            Err(e) => {
                println!("Error when reading XML: {}", e.display());
                break;
            }
            Ok(e) => {
                match e {
                    Event::Eof => {
                        println!("End of file reached");
                        break;
                    }
                    Event::Start(bs) => {
                        path.push(bs.get_tag_name(&reader));
                        print_path(&path, options);
                        print_bytes_start(&reader, bs, options, "StartTag");
                    }
                    Event::End(be) => {
                        print_path(&path, options);
                        path.pop(); // quick-xml checked closing for us
                        println!("EndTag {}", reader.decode(be.name()));
                    }
                    Event::Empty(bs) => {
                        path.push(bs.get_tag_name(&reader));
                        print_path(&path, options);
                        print_bytes_start(&reader, bs, options, "EmptyTag");
                        path.pop();
                    }
                    Event::Text(bt) => {
                        print_path(&path, options);
                        print_bytes_text(&reader, bt, options, "Text");
                    }
                    Event::CData(bt) => {
                        print_bytes_text(&reader, bt, options, "CData");
                    }
                    Event::Comment(bt) => {
                        print_bytes_text(&reader, bt, options, "Comment");
                    }
                    Event::PI(bt) => {
                        print_bytes_text(&reader, bt, options, "ProcessInstruction");
                    }
                    Event::DocType(bt) => {
                        print_bytes_text(&reader, bt, options, "DocType");
                    }
                    Event::Decl(bd) => {
                        print!("XmlDecl");

                        let version = bd.version();
                        match version {
                            Err(_) => print!(" [Bad version]"),
                            Ok(v) => print!(" Version:{}", reader.decode(v)),
                        }

                        let encoding = bd.encoding();
                        if let Some(encoding) = encoding {
                            match encoding {
                                Err(_) => print!(" [Bad encoding]"),
                                Ok(enc) => print!(" Encoding:{}", reader.decode(enc)),
                            }
                        }

                        let standalone = bd.standalone();
                        if let Some(standalone) = standalone {
                            match standalone {
                                Err(_) => print!(" [Bad standalone]"),
                                Ok(st) => print!(" Standalone:{}", reader.decode(st)),
                            }
                        }

                        println!();
                    }
                }
            }
        }
    }
}

fn print_path(path: &[String], options: &Options) {
    if path.len() > 0 && !options.no_path {
        print!("{}, ", path.join(" -> "));
    }
}

/// Print content in `BytesText`
fn print_bytes_text<'a, B>(
    reader: &Reader<B>,
    bt: BytesText<'a>,
    options: &Options,
    event_name: &str,
) where
    B: BufRead,
{
    let content = bt.unescaped();
    match content {
        Err(_) => {
            println!("{} [Broken content]", event_name);
        }
        Ok(t) => {
            let content = reader.decode(&t);
            if options.text_width.is_none() {
                println!("{} [{}]", event_name, content);
            } else {
                let width = options.text_width.unwrap() as usize;
                if content.chars().count() <= width {
                    println!("{} [{}]", event_name, content);
                } else {
                    println!(
                        "{} [{}...]",
                        event_name,
                        content.chars().take(width).collect::<String>()
                    );
                }
            }
        }
    }

}

/// Print content in `BytesStart`, including attributes of the tag
fn print_bytes_start<'a, B>(
    reader: &Reader<B>,
    bs: BytesStart<'a>,
    options: &Options,
    event_name: &str,
) where
    B: BufRead,
{
    println!("{} {}", event_name, reader.decode(bs.name()));

    if !options.no_attr {
        for attr in bs.attributes() {
            if attr.is_err() {
                println!("\t[BrokenAttr]");
            } else {
                let attr = attr.unwrap();
                let key = reader.decode(attr.key);
                let value = attr.unescaped_value();
                if value.is_err() {
                    println!("\tAttr {} = [BrokenValue] ", key);
                } else {
                    println!("\tAttr {} = [{}] ", key, reader.decode(&value.unwrap()));
                }
            }
        }
    }
}

trait Taggable {
    fn get_tag_name<B>(&self, reader: &Reader<B>) -> String
    where
        B: BufRead;
}

impl<'a> Taggable for BytesStart<'a> {
    fn get_tag_name<B>(&self, reader: &Reader<B>) -> String
    where
        B: BufRead,
    {
        String::from(reader.decode(self.name()).to_owned())
    }
}

//
// Command line arguments
//
fn get_options_from_command_line() -> Options {
    use clap::Arg;

    let input_file_arg = Arg::with_name("input")
        .takes_value(true)
        .value_name("FILE")
        .short("i")
        .long("input")
        .help("Name of input file. Omit to input from STDIN")
        .validator(|s| {
            let path = std::path::Path::new(&s);
            if path.exists() && path.is_file() {
                Ok(())
            } else {
                Err("Input should be an existing file".to_owned())
            }
        });

    let no_path_arg = Arg::with_name("no_path")
        .takes_value(false)
        .long("no-path")
        .help("Do not print path of XML nodes");

    let no_attr_arg = Arg::with_name("no_attr")
        .takes_value(false)
        .long("no-attr")
        .help("Do not print attributes of XML tags");

    let text_width_arg = Arg::with_name("text_width")
        .takes_value(true)
        .value_name("WIDTH")
        .long("text-width")
        .help(
            "Limit length of printed text. Default is not to pose any limitation.",
        )
        .validator(|s| {
            s.parse::<u32>()
                .map_err(|_| "Should be a positive integer".to_owned())
                .map(|_| ())
        });

    let matches = App::new("quick-xml-tool")
        .arg(input_file_arg)
        .arg(no_path_arg)
        .arg(no_attr_arg)
        .arg(text_width_arg)
        .get_matches();

    Options {
        input_file: matches.value_of("input").map(|x| x.to_owned()),
        no_path: matches.is_present("no_path"),
        no_attr: matches.is_present("no_attr"),
        text_width: matches.value_of("text_width").map(|s| s.parse().unwrap()),
    }
}

struct Options {
    pub input_file: Option<String>,
    pub no_path: bool,
    pub no_attr: bool,
    pub text_width: Option<u32>,
}
