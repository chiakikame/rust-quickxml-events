# Rust `quick-xml` event dump

A little tool transforming XML file into series of `quick-xml` events. It's
designed for learning about how `quick-xml` works.

## License

This program is licensed under GNU GPLv3.

## Requirement

A building environment for Rust programming language.

## Usage

This program fetch XMl data from `stdin`. Example usage:

```
$ cat myData.xml | ./rust-quickxml-events | less
``` 
